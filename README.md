# lander.cat
Hello, I am Facundo Lander and I made this website by myself.
It was quite a travel, a sane person would say a static personal site doesn't need all of what I made on this site, but I think it was very fun to make it, I also learned that making a WYSIWYG editor is not that hard after all!
Of course, like in every development project for every developer that ever existed since apes developed a hammer, there are little things that I would love to know a better way to do, but it's the best I came up with at the moment, and I'm still very proud of my code.

I really, really hope someone else than me enjoys the website I made with all my love and hopefully I help someone else learn a new thing or help them make their own personal site!
If my page helped you, please, please contact me and tell me, it would really make me really happy.

## Requisites
Any LAMP server works.

## Licenses
#### Code
lander.cat is free software: you can redistribute it and/or modify
it under the terms of the **GNU General Public License** as published by
the Free Software Foundation, either **version 3** of the License, or
(at your option) **any later version**.

lander.cat is distributed in the hope that it will be useful,
but WITHOUT ANY WARRANTY; without even the implied warranty of
MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
GNU General Public License for more details.

You should have received a copy of the GNU General Public License
along with lander.cat.  If not, see <https://www.gnu.org/licenses/>.

#### Content
All the content posted of my website lander.cat is, unless otherwise noted, licensed under
The Creative Commons Attribution-ShareAlike 3.0 (**CC BY-SA 3.0**).
