<?php
/*
	Copyright 2021 Facundo Lander

	This file is part of lander.cat.

	lander.cat is free software: you can redistribute it and/or modify
	it under the terms of the GNU General Public License as published by
	the Free Software Foundation, either version 3 of the License, or
	(at your option) any later version.

	lander.cat is distributed in the hope that it will be useful,
	but WITHOUT ANY WARRANTY; without even the implied warranty of
	MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
	GNU General Public License for more details.

	You should have received a copy of the GNU General Public License
	along with lander.cat.  If not, see <https://www.gnu.org/licenses/>.
*/

	require "../../lib/lander.php";
	$TRANSLATION->load("pages/gallery");

	if (isset($_POST["id"])) {
		if ($_POST["pass"] === CONFIG["gallery"]["password"]) {
			$id = $DB->escape($_POST["id"]);
			$DB->query("DELETE FROM gallery WHERE id = $id");
		}
		header("Location: /gallery/list");
		die();
	}

?>
<!DOCTYPE html>
<html lang="<?php echo $TRANSLATION->locale ?>">
<head>

	<?php echo META ?>
	<meta name="robots" content="noindex">

	<title>Facundo Lander</title>

	<?php echo INCLUDES ?>
	<link rel="stylesheet" type="text/css" href="/css/editor.css">

</head>
<body>

	<?php echo HEADER ?>

	<main>
		<a href="/gallery/list" id="back"><?php echo $TRANSLATION["back"] ?></a>
		<?php
			$id = $DB->escape($_GET["id"]);
			$picture = $DB->query("
				SELECT filename
				FROM gallery
				WHERE id = $id
			")[0];
		?>
		<h1>Delete <?php echo $picture["filename"] ?></h1>
		<img src="/img/<?php echo $picture["filename"] ?>">
		<p style="width: 40%; text-align: left; margin: 0 auto; margin-top: 2em;">
			Are you 100% sure you want to delete this picture?
			Remember: I haven't coded any trash bin or backup system!!!!! :O
		</p>
		<form method="POST">
			<input type="hidden" name="id" value="<?php echo $id ?>">
			<input type="password" name="pass" placeholder="Password">
			<input type="submit" value="Delete picture!">
		</form>
	</main>

	<?php echo FOOTER ?>

</body>
</html>
