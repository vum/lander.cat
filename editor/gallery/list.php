<?php
/*
	Copyright 2021 Facundo Lander

	This file is part of lander.cat.

	lander.cat is free software: you can redistribute it and/or modify
	it under the terms of the GNU General Public License as published by
	the Free Software Foundation, either version 3 of the License, or
	(at your option) any later version.

	lander.cat is distributed in the hope that it will be useful,
	but WITHOUT ANY WARRANTY; without even the implied warranty of
	MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
	GNU General Public License for more details.

	You should have received a copy of the GNU General Public License
	along with lander.cat.  If not, see <https://www.gnu.org/licenses/>.
*/

	require "../../lib/lander.php";
	$TRANSLATION->load("pages/gallery");
?>
<!DOCTYPE html>
<html lang="<?php echo $TRANSLATION->locale ?>">
<head>

	<?php echo META ?>
	<meta name="robots" content="noindex">

	<title>Facundo Lander</title>

	<?php echo INCLUDES ?>
	<link rel="stylesheet" type="text/css" href="/css/editor.css">

</head>
<body>

	<?php echo HEADER ?>

	<main>
		<a href="/gallery" id="back"><?php echo $TRANSLATION["back"] ?></a>
		<?php
			$pictures = $DB->query("
				SELECT id, filename, post_date 
				FROM gallery
				ORDER BY post_date DESC, id DESC
			");
		?>
		<a href="/gallery/new">Add a new picture</a>
		<table>
			<thead>
				<tr>
					<th>Picture</th>
					<th>Filename</th>
					<th>Post date</th>
					<th>Actions</th>
				</tr>
			</thead>
			<tbody>
				<?php foreach ($pictures as $picture): ?>
					<tr>
						<td><img src="/img/<?php echo $picture["filename"] ?>"></td>
						<td><?php echo $picture["filename"] ?></td>
						<td><?php echo $picture["post_date"] ?></td>
						<td>
							<a href="/gallery/edit/<?php echo $picture["id"] ?>">Edit</a>
							<a href="/gallery/delete/<?php echo $picture["id"] ?>">Delete</a>
						</td>
					</tr>
				<?php endforeach ?>
			</tbody>
		</table>
	</main>

	<?php echo FOOTER ?>

</body>
</html>
