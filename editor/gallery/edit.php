<?php
/*
	Copyright 2021 Facundo Lander

	This file is part of lander.cat.

	lander.cat is free software: you can redistribute it and/or modify
	it under the terms of the GNU General Public License as published by
	the Free Software Foundation, either version 3 of the License, or
	(at your option) any later version.

	lander.cat is distributed in the hope that it will be useful,
	but WITHOUT ANY WARRANTY; without even the implied warranty of
	MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
	GNU General Public License for more details.

	You should have received a copy of the GNU General Public License
	along with lander.cat.  If not, see <https://www.gnu.org/licenses/>.
*/

	require "../../lib/lander.php";
	$TRANSLATION->load("pages/gallery");

	$id=0;

	if (isset($_POST["id"])) {

		if ($_POST["pass"] === CONFIG["gallery"]["password"]) {
			$filename = $DB->escape($_FILES["picture"]["name"]);
			$filedata = base64_encode(file_get_contents($_FILES["picture"]["tmp_name"]));
			if ($_POST["id"] == 0 and isset($_FILES["picture"])) {
				$DB->query("INSERT INTO gallery (filename, filedata) VALUES ('$filename', FROM_BASE64('$filedata'))");
				$insert_id = $DB->getID();
				foreach ($TRANSLATION->provided as $locale) {
					$title = $DB->escape($_POST["title"][$locale]);
					$body = $DB->escape($_POST["body"][$locale]);
					$DB->query("
						INSERT INTO gallery_translation VALUES
							($insert_id,
							'$locale',
							'$title',
							'$body')
					");
				}
			} else {
				$id = $DB->escape($_POST["id"]);
				foreach ($TRANSLATION->provided as $locale) {
					$title = $DB->escape($_POST["title"][$locale]);
					$body = $DB->escape($_POST["body"][$locale]);
					$DB->query("
						UPDATE gallery_translation SET
							title = '$title',
							body = '$body'
						WHERE id = $id AND code = '$locale'
					");
				}
			}
		}

		header("Location: /gallery/list");
		die();

	} elseif (isset($_GET["id"])) {

		$id = $DB->escape($_GET["id"]);
		$translations = $DB->query("SELECT * FROM gallery_translation WHERE id = $id");


		$translation = Array();
		foreach ($translations as $t)
			$translation[$t["code"]] = Array("title" => $t["title"], "body" => $t["body"]);

	} else {
		$translation = Array();
		foreach ($TRANSLATION->provided as $locale)
			$translation[$locale] = Array("title" => "", "body" => "");
	}
?>
<!DOCTYPE html>
<html lang="<?php echo $TRANSLATION->locale ?>">
<head>

	<?php echo META ?>
	<meta name="robots" content="noindex">

	<title>Facundo Lander</title>

	<?php echo INCLUDES ?>
	<link rel="stylesheet" type="text/css" href="/css/editor.css">
	<script type="text/javascript" src="/js/wysiwyg.js" defer></script>

</head>
<body>

	<?php echo HEADER ?>

	<main>
		<a href="/gallery/list" id="back"><?php echo $TRANSLATION["back"] ?></a>
		<form method="POST" enctype="multipart/form-data">
			<input type="hidden" name="id" value="<?php echo $id ?>">
			<?php if (!isset($_GET["id"])): ?>
				<h1>Add a new picture</h1>
				<input type="file" name="picture">
			<?php else: ?>
				<h1>Edit picture text</h1>
			<?php endif ?>
			<?php foreach ($TRANSLATION->provided as $locale): ?>
				<fieldset>
					<legend><?php echo $locale ?></legend>
					<input value="<?php echo $translation[$locale]["title"] ?>"
						placeholder="title" type="text" name="title[<?php echo $locale ?>]">
					<textarea class="wysiwyg" name="body[<?php echo $locale ?>]"
						><?php echo $translation[$locale]["body"] ?></textarea>
				</fieldset>
			<?php endforeach ?>
			<input type="password" name="pass" placeholder="Password">
			<input type="submit" value="<?php echo (!isset($_GET["id"]) ? "Upload" : "Edit") ?> picture!">
		</form>
	</main>

	<?php echo FOOTER ?>

</body>
</html>
