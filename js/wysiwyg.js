
/**
 *
 * @source: https://lander.cat/js/wysiwyg.js
 *
 * @licstart  The following is the entire license notice for the 
 *  JavaScript code in this page.
 *
 * Copyright (C) 2021 Facundo Lander
 *
 *
 * The JavaScript code in this page is free software: you can
 * redistribute it and/or modify it under the terms of the GNU
 * General Public License (GNU GPL) as published by the Free Software
 * Foundation, either version 3 of the License, or (at your option)
 * any later version.  The code is distributed WITHOUT ANY WARRANTY;
 * without even the implied warranty of MERCHANTABILITY or FITNESS
 * FOR A PARTICULAR PURPOSE.  See the GNU GPL for more details.
 *
 * As additional permission under GNU GPL version 3 section 7, you
 * may distribute non-source (e.g., minimized or compacted) forms of
 * that code without the copy of the GNU GPL normally required by
 * section 4, provided you include this license notice and a URL
 * through which recipients can access the Corresponding Source.
 *
 * @licend  The above is the entire license notice
 * for the JavaScript code in this page.
 *
 */


// Please take into consideration I coded this at 5AM lol


const LANDER_PEN_STYLE = `
	div.lander-pen {
		border: 3px solid #333;
		margin: 1em;
		border-radius: 4px;
	}
	div.lander-pen > textarea {
		display: none;
	}
	div.lander-pen.hacker > textarea {
		display: block;
	}
	div.lander-pen > div {
		background-color: #333;
		padding: .3em;
	}
	div.lander-pen > div > img {
		width: 32px;
	}
	div.lander-pen > iframe {
		width: 100%; height: 100%;
		border: none;
	}
	div.lander-pen.hacker > iframe {
		display: none;
	}
`;

const LANDER_PEN_CONTROLS = `
	<img src="data:image/png;base64,
		iVBORw0KGgoAAAANSUhEUgAAACAAAAAgCAYAAABzenr0AAAACXBIWXMAAC4jAAAuIwF4pT92AAAD
		cUlEQVRYR+WXyWsUQRSHM0lMQty3iNshuByMG7iAKGJwOwQi4kFE/wDxoMKoFz2KF8GjiBdFxKNB
		omJA0YMLHqJiXBKXhLiQiKIxY8w6id9vqNayU93TPQl48MFHdddU9Xv16r1XNXl5/7sk/A4YGhoK
		88lEflwCG2EZTII+aIO7UJNIJL6OyKkywEExfTvgNnTCAAyCRK3ohY9wEqbnbESAAbX0d1hKjW5n
		84Pew6NpwBE+2OdT5a3aa/2W1NNRHMWIKDHwng/NggH4Ce3wwuz7GFq5exXMBu97GrOZeHiWzYjC
		LAOq+b0ULsBDqIdGSFnzyniuNQZ43V089GRT7vzdFwM3eG+BsoDYKKf/KvRbe5Dm+QwUjNSAChPV
		92kLfQZM5X0/NIIU2tLEy9KclGuSpegAzym4aPWV8LwTHoDSzi9fzO/DYivIoLAY2MQkBdljM3kR
		7XHYYuLCr0RBqoKkgJ0L73LyglltPm0rdME2OApaXZioOIkeaIMrMD+bEUFpuJaJ16AfOqEcgtya
		5rfv8BJewTRYDyXmvYp0VCpHE+OBg7TdBteqVYBU8W7BXlgMpWZugjZpvKc4OQ350bT/CcJLTFJ0
		2+klQ+RiRfkhUAoWGKW/g1eK6CuC5yBD22FBXAOeGAOaaXX4fIObUKnV+JXa754iM14GqIzvCzLA
		lQUzGDwZdMyeB1XAZnitj7CfURcz3gyU+wM94DJAQTQWBqEB6qJqtFavoJ0ne0EGaEFOcQXHHEaq
		/ssAHSq5SNJSqhtORxwDdMvRJJ18n+NqZ79XMGe7Wbmmq0DpAHOKawsmGNfJgGGWK+BCREfyKZhp
		jdE37sTxgPZLhvUaojihiEEq0ZdhnVmA5qlIXYe3cTygIFTw6MyXF8JE3toKu6ESFPl2mrTwfoLM
		UUV1imsLpFgTdLvVCjwZx4OieyGshOVQAboR6frlz0/FTxLlmfSNY8AHBneDTjOdiGtgtVGs249q
		vHffCyoKyp5joJtSPCHIdObvgTrQCahy7F3BVY7DRFWvAaoh23UvY5hrkO5yqlwbQMEVJF46eJfV
		VgbWwDncrudIEnQcT2H2Wagy7tY4KVRx0vbo+FVkvwFdWJ6aNhWjVGcMDLuWK6d3gf6Cae+VEY/g
		HjTBJ9Dq/5LRNMD+sLZCGZGOqyDSPvzLQb8Aaox5I74YndsAAAAASUVORK5CYII="

		onclick="this.parentElement.parentElement.setStyle('bold')"
	>
	<img src="data:image/png;base64,
		iVBORw0KGgoAAAANSUhEUgAAACAAAAAgCAYAAABzenr0AAAACXBIWXMAAC4jAAAuIwF4pT92AAAC
		hklEQVRYR+2XyWsUQRSHM8YtblGJKC6IBkRBBfEqQjwoQTTiyYOe/R/Ev0D8EzyIJ8WDcQGPioJ4
		EwTBKIoHzYIkmgWzZ/w+qIZhpmemq2ZuWvBRvVS99+t63fVed3T8660UuwDlcjmbspmDNbAORrKL
		pVKcyZrRFQ4qtTnuMvTBUegJzhUgC/Ar8IN+FKZhDN7CZ4TN5z1sEQHdTHwCx2EtdMJScOpyKEA7
		sjqcL9OvBIdT9HcQcCNPgBOatUsM2AJv4L1PA99hMggxBFcDz+g3wk7oAu+tb+Yg6r4hqqKP83GY
		g+sao++CDdAD+0Eh7Wk5Ah5ybRGm4Ex7vDSwUiWgl/MxWIEh2BErYFXshKrxVzjf7qrDa/gZa69V
		ARdwqI1ZeBrrPGl8RQiOcTwRlv8b/e46e0hDP62sgMvv52n7AMMpT5QqwG24Pyz/Iv3LFOfOSRVw
		grm9wanx9wVMaqkCLuJtE7j9uvcbgqSWIsDcYFLSuTnhBZh4klqKgCN4OhgE/KF/nOQ5TEoRcC4s
		vyY+watWBNRkwybfsnE/D6Zk8/v90CdriF2Bk3g6FLxZdAwme04MwTXmuQdYcDyCL60KqJmfk26z
		/H+Ae9NgG4XDeWNjBcWE4BTGrW7MfM/hY6yzvPGNBHhvD2wDjwdC76d3rx3OtZFXlG7l+k04G+Lt
		OGO+DyxKZ+ABWPH6IloDjMNXmKD4tDpOb8T1FiyEWNfrlrixDPMwCzMwAu/gtqm5qIK8ENxlstXv
		HBjvrLzObHotK8Od757gfuL7YXr2n6Hw1lzvv2AXRk6DL97eYNRS21C49Jbk1vv+ERmG3zDkMSEw
		FIVbkR8TjWVP3IkD8///1rYV+AtaLV7ik5qEpwAAAABJRU5ErkJggg=="

		onclick="this.parentElement.parentElement.setStyle('italic')"
	>
	<img src="data:image/png;base64,
		iVBORw0KGgoAAAANSUhEUgAAACAAAAAgCAYAAABzenr0AAAACXBIWXMAAC4jAAAuIwF4pT92AAAC
		3ElEQVRYR+2WTW9OQRTHW9p6bcVLqCASmkai4i0pgp2NICUsJMLKF/ARrH0FPgJ2pRuLbrBhVY0N
		TSNppRU0qi1tH7+f3Ptkcp97n3nuw4qe5J85M/ecM/85M/fMtLSsyL+WgUql0pKgN9DTsZrlrsqO
		ZJy66Z8GHel4iYRdwLY7Zl9DIHBYi/4QDIFrsUA5308xdi7mV4/ADZyPgjXgUCxQ5vs6+ifA5Zhf
		EYFWHK8Cs7Aa7I4Fynw/Tn8z6AOb6vkWEdiO034gEWG/jOzF2Nhmz2wUShEBV92ZeFVot4G2EgwO
		Y9sB1oONzRDoSgLoawa2gi0lCPQnhNtp3cbSGZjDYzHw2oDunjYiezByC5RlsNQMAR0XAkfT2NvI
		7NgcA54ZMxeVojMwgeenwNtUeqIbkUsYpQfvO/pMMxkw/aPAA6hI9KRtXnkNxjwrFiBF3ynwtRkC
		+rwA6TkwnRajWGk9j80+kKZ/BP1bswSGcQy3wX21wBSJ23Qb+O8r82Cw3uS534J0tqE/ActAsX0E
		HK/eboF+E302sbUZATuztlFCGYcB+jNB0Cn0szkEDjI2FtjNo9/JI1qWQDtBngZZWEIfAp1B8B70
		1xmbQfpdf4OAqe4H45nV3Uu24gztKJCY4ja9ApLK26ZoAnKdCHYLTASrnEN/D9LtceKf4CXoK5rc
		8agUOHvwroA3YCEhkq56kf40uA92RSfIGNSUywjLHvwvAp9b3vMu6Tl4DPxtK62tDVXgKo2yBLTf
		AWytjpbcL8DLa9aof0SA1R8gxnXghWJZ9SpNX0XG901gwUmJ2/8BrPkWnmnwFrwDk+Aj+JyQ+6Bd
		luDvQEzs4+MuGACu0ElNr7eijxPFa9VV65OWaPvqQjvtzYakJOqjxDiSk4iExsAzMAyZ8eoWQCJ9
		fB7ho/e5979BwlV6RRtMOJF9V2+9l4S6WyERF+E58TXlAu2nC5DsJAQe0K7If56BX90feMBjyD1m
		AAAAAElFTkSuQmCC"

		onclick="this.parentElement.parentElement.setStyle('underline')"
	>
	<img src="data:image/png;base64,
		iVBORw0KGgoAAAANSUhEUgAAACAAAAAgCAYAAABzenr0AAAACXBIWXMAAC4jAAAuIwF4pT92AAAC
		vklEQVRYR+2WO2tUQRSAs5pETRRMEdTgK7aKURAMiI0KgqidIIhFsLK2sMo/SGdvI7EKBCV9wEox
		Kvh+QqIG30LM+7Xr98G9cPfu7O7dxTQmAx8zd+6ZmTPnnDkzDQ1rZbVbIJc2QKFQqGSTdfzcBYfh
		EOyHdtgAzbAIj+EGvApNlMuVLFkspgIBcvQdg374DDOwCHmYgwn4BT9gHJ7D8dBcaaUaM4RAEzJX
		oBe2g1bQTAvwHoZgGEZhDqbhJyxnmLtUJKD1Rfq+QVyWaYzBNdiclq9r0eSggAKDkalVQJO/gyNl
		XFXz+pqzWtmWEMjT7oeRaoOy/s+igGEbh67ynoBNWReoJpdFAQPM42VRkdPQBy3VJq/rf8C3HfQ9
		TcSBsTANA9ANB6EtHlfroqFEtJFJTkE37IUO2Am7wYSTLB5H40ILTYHHbwI8jjPwG77AGxiFFyQi
		v8OFXZyDJzAJHjejvt7iWHGeeZgCj+8wnC3RgE5N/aze1WoYp1JmyqMqkc6EO8oZh/7YXZrddroO
		DdU9FmUNeL9t664lfxTFAFqdp+8qeME4YBbmQR/HCzqJmIr9Z21phQPQCaZvZR7CAzAt22ft4reJ
		hZfRuOIqiuZG6i1R24sovqCS7dCl1YxsL3hK9P09aEqMD675TztZTCVcWF9/gK2VFMiSiGpVUJcM
		gj7WLW2VJlgJBVzP2NDfxpB5oWxZCQVMXD3g3B/BpJS9JPzVHvnwFvVJaE36MtXW753QAyMwCyae
		y+kxaU0qvYgmEb4PF+AMjMEjeBvtyrTscd0He8B0rb/Xg/6/CQPZtx5JpjQ2gvvge7SrBeolsDa9
		2va4xWnb96GRfx1aQhZLK5T1VdzFwEtwItqtF5Y+FgPuD3yCu3AHXpfbefpVnFWBeD7NHpvb94AK
		eLt9hXHIV3121+yTtQH/uwX+AvptO3nkEYMkAAAAAElFTkSuQmCC"

		onclick="this.parentElement.parentElement.setStyle('strikethrough')"
	>
	<img src="data:image/png;base64,
		iVBORw0KGgoAAAANSUhEUgAAACAAAAAgCAYAAABzenr0AAAACXBIWXMAAC4jAAAuIwF4pT92AAAD
		FElEQVRYR+2WX2iOURzHt1myyfyJtDTNtHJhJBMXonbjahck/5MLSblyp5UmQlJy44Ibyg2lJApJ
		IW4WhYgLuxAxWzI2Nob5fNZ517vH8z7P+75bSe2tT8/znnN+v9/3/M7vnPOUlIz//vcMDA4OToOz
		UFHMXMqKMYrY7OZ/LfQV4ytVADPbAudhEYyIwf8qGtZDWzHBE20MBtvgO/i7GSOgmbYuaNYZz4Vh
		OXbxTJ1cTgEYl4JOvoXgP3keiRrQdgY6oS4IOMy7Y3vhMtRERadmCoNy2AtfQ3AzoOMJMQJe0v5U
		wUHA6iBIU4Xcg3l5i2DgRGiFTNp7eN8Of6WTNtPt7C9kB+D9APTBb/gFD6A2LxEMagrGzqAb1uRK
		GX1b4SPsjwio5P+VkAFFmIlH0Jgogs7ZYEo1eg8rk9aL/hPwCTZEBFiMc+F+CO5kMj5dyimxfum4
		Clb0KZiZViyMuRuytDQ6M/9DHdyGzHIq5Ae8gKbofm6l8Q0cgrKow5jiM8BreAc1cWKDCE/Jo2Ct
		mAV/Pk9nbDLFVU3DVOhPm3nob+BZCR+gI86mtHRoY3RDC+wI755kvXA9mgHX/wlYVJvyyMC6kH73
		eqJm+t1ZF2EA3NoHc9XAxiDANTIjOX/0u9V0djxJQFiGnTy/gOt/AyYlObYQ3X7HUhxfo78fPCOS
		/C2g/1WY/VuejWnpWsIgi1Gj+lyD6XsG7pjFuQTQPhmckDP3SN+XJHY4FoPcCZ6A56A8Zgd4tFr9
		LlVVgoAW+rwTXHtPSwWl1ziDKoJzZzjikNGatrXwGW7BUKnHiFxOXwd4Ct6BOQbPS0AIYuFYZM+h
		PtuQd09AD5eTcQ5pmwEPwdR7GXkg5R88CDALl4ITvwOqM8F4toEF6HUdN3uPW7ezV7Xbu7DgGY9B
		ucHMxOYgzFvNananrMohwMtpGfhNUVzwLBENONkD04OAFbx7Abm+8+MEpFfZKEYQ0AL0rm+HWaNw
		NWxa6HdbD5YD0Ald/0KAl4uf3+1jEVwfxWTAmT8eKwEF+2Ht3ed+uBZsO24Ql4E/dFn8t0/gb9QA
		AAAASUVORK5CYII="

		style="float:right"

		onclick="this.parentElement.parentElement.classList.toggle('hacker')"
	>
`;

class Editor {

	constructor(textarea) {

		// Don't even ask me why
		if (textarea.value == "")
			textarea.value = "<br>";

		this.lander_pen = document.createElement("div");
		//this.lander_pen.classList.add("lander-pen"); wtf?!??!!?
		this.lander_pen.style.width = textarea.offsetWidth+"px";
		this.lander_pen.style.height = "calc(40px+"+textarea.offsetHeight+"px)";

		this.lander_pen.appendChild(document.createElement("div"));
		this.lander_pen.appendChild(textarea.cloneNode());
		this.lander_pen.appendChild(document.createElement("iframe"));

		this.data = this.lander_pen.querySelector("textarea");
		this.data.value = textarea.value;
		this.controls = this.lander_pen.querySelector("div");
		this.controls.innerHTML = LANDER_PEN_CONTROLS;
		this.editor = this.lander_pen.querySelector("iframe");

		let updateData = this.updateData.bind(this);
		let editor = this.editor;
		this.lander_pen.setStyle = function(style) {
			editor.contentDocument.execCommand(style);
			editor.focus();
			updateData(editor.contentDocument.body.innerHTML);
		}

		textarea.parentElement.replaceChild(this.lander_pen, textarea);

		let initialD = this.data.value;
		this.editor.onload = function() {
			this.parentElement.classList.add("lander-pen");
			this.contentDocument.designMode = "on";
			this.contentDocument.body.innerHTML = initialD;
			//this.contentDocument.body.style.backgroundColor = "#444";
			this.contentDocument.body.style.color = "#ccc";
			this.contentDocument.onkeyup = function() {
				updateData(this.body.innerHTML);
			}
		}

		this.data.onkeyup = this.data.onchange = function() {
			editor.contentDocument.body.innerHTML = this.value;
		}

		let css = document.createElement("style");
		css.innerHTML = LANDER_PEN_STYLE;
		document.head.appendChild(css);

	}

	updateData(data) {
		this.data.value = data;
	}

}

let editors = document.getElementsByClassName("wysiwyg");
for (var i = 0; i < editors.length; i++)
	new Editor(editors[i]);
