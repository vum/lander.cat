<?php
	require "lib/lander.php";
	$TRANSLATION->load("pages/index");
?>
<!DOCTYPE html>
<html lang="<?php echo $TRANSLATION->locale ?>">
<head>

	<?php echo META ?>

	<title>Facundo Lander</title>

	<?php echo INCLUDES ?>

</head>
<body>

	<?php echo HEADER ?>

	<main>
		<img src="/img/pp.jpeg" class="right" alt="<?php echo $TRANSLATION["meta"]["petit"] ?>">
		<p>
			<?php echo $TRANSLATION["intro"] ?> 
		</p>
		<p>
			<?php echo $TRANSLATION["navigation"] ?> 
		</p>
		<p>
			<?php echo $TRANSLATION["sellout"] ?> 
		</p>
	</main>

	<?php echo FOOTER ?>

</body>
</html>
