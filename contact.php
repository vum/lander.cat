<?php 
	require "lib/lander.php";
	$TRANSLATION->load("pages/contact");
?>
<!DOCTYPE html>
<html lang="<?php echo $TRANSLATION->locale ?>">
<head>

	<?php echo META ?>

	<title><?php echo $TRANSLATION["title"] ?></title>

	<?php echo INCLUDES ?>
	<link rel="stylesheet" type="text/css" href="/css/contact.css">

</head>
<body>

	<?php echo HEADER ?>

	<main>
		<a href="/" id="back"><?php echo $TRANSLATION["back"] ?></a>
		<h2><?php echo $TRANSLATION["biotitle"] ?></h2>
		<!-- NO!!! Tables aren't old school!!!! -->
		<table>
			<?php foreach ($TRANSLATION["bio"] as $bio): ?> 
				<tr>
					<td><?php echo $bio["title"] ?></td>
					<td><?php echo $bio["text"] ?></td>
				</tr>
			<?php endforeach ?> 
		</table>
		<h2><?php echo $TRANSLATION["contact"] ?></h2>
		<div class="wave">
			<a href="https://twitter.com/facundolander" class>Twitter</a>
			<a href="https://www.youtube.com/channel/UCiWtMIpp1AtVvx7eaPGa-yw" class>YouTube</a>
			<a href="https://www.linkedin.com/in/facundo-lander/" class>LinkedIn</a>
			<a href="https://gitlab.com/vum" class>GitLab</a>
		</div>
		<p class="note">
			<?php echo $TRANSLATION["email"] ?> 
		</p>
	</main>

	<?php echo FOOTER ?>

</body>
</html>
