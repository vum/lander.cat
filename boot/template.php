<?php
	require "lib/lander.php";
	$TRANSLATION->load("pages/index");
?>
<!DOCTYPE html>
<html lang="<?php echo $TRANSLATION->locale ?>">
<head>

	<?php echo META ?>

	<title>Facundo Lander</title>

	<?php echo INCLUDES ?>

</head>
<body>

	<?php echo HEADER ?>

	<main></main>

	<?php echo FOOTER ?>

</body>
</html>
