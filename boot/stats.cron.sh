#!/bin/bash

(zcat /var/log/apache2/access.log.*.gz & cat /var/log/apache2/access.log) | \
	grep "/lander.cat\|/www.lander.cat" | \
	goaccess - -o /var/www/lander.cat/stats.html --log-format=COMBINED --real-os
