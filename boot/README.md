## Apache
The virtual host needs the AllowOverride option set on all on the document root folder.

    <Directory /var/www/lander.cat>
        AllowOverride all
    </Directory>

## MySQL
First create the database and user

    CREATE DATABASE lander;
    CREATE USER user@localhost IDENTIFIED WITH mysql_native_password BY 'password';
    GRANT ALL PRIVILEGES ON lander.* TO user@localhost;

Then load the schema from lander.sql like so:

    mysql -uuser -ppassword lander < lander.sql

## Cron
The file `stats.cron.sh` generates the file $document_root/etc/stats.html with the log analyzer tool "goaccess", to execute every day, place it into de /etc/cron.daily folder.

## Finally
You have to edit the file $document_root/etc/config.ini with the proper database information and a made up password for the gallery upload tool at `GET /gallery/list`.