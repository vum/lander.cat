# lander.cat database by Facundo Lander
# MySQL


# SCHEMA
CREATE TABLE gallery (
	id SERIAL PRIMARY KEY,
	filename VARCHAR(255),
	filedata MEDIUMBLOB,
	post_date DATE DEFAULT (CURRENT_DATE)
);
CREATE TABLE gallery_translation (
	id BIGINT REFERENCES gallery (id) ON DELETE CASCADE,
	code CHAR(2) NOT NULL,
	title VARCHAR(255),
	body TEXT,
	PRIMARY KEY (id, code)
);


# PROCEDURES
DELIMITER $$
	CREATE PROCEDURE get_gallery_translation(v_id BIGINT, v_code CHAR(2))
	BEGIN
		SELECT * FROM gallery_translation WHERE id = v_id AND code = v_code;
	END;
$$
DELIMITER ;
