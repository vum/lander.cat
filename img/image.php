<?php

require "../lib/lander.php";

if (isset($_GET["filename"])) {

	$filename = $DB->escape($_GET["filename"]);
	$file = $DB->query("
		SELECT filedata, OCTET_LENGTH(filedata) AS length, MD5(filename) AS etag
		FROM gallery
		WHERE filename = '$filename'"
	);

	if (sizeof($file) == 1) {
		$file = $file[0];
		$extension = end(explode(".", $filename));
		switch ($extension) {
			case "jpg": $type="jpeg"; break;
			case "svg": $type="svg+xml"; break;
			default: $type=$extension;
		}

		header("Content-Type: image/$type");
		header("Cache-control: max-age=1209600");
		header("Expires: ".gmdate(DATE_RFC1123, time()+1209600));
		header("Content-Length: $file[length]");
		// https://developer.mozilla.org/en-US/docs/Web/HTTP/Headers/ETag
		header("ETag: W/\"$file[etag]\"");

		if (isset($_GET["download"]))
			header("Content-Disposition: attachment");

		echo $file["filedata"];

	} else http_response_code(404);

} else http_response_code(404);

?>
