<?php
	require "lib/lander.php";
	$TRANSLATION->load("pages/gallery");
?>
<!DOCTYPE html>
<html lang="<?php echo $TRANSLATION->locale ?>">
<head>

	<?php echo META ?>

	<title><?php echo $TRANSLATION["title"] ?></title>

	<?php echo INCLUDES ?> 
	<link rel="stylesheet" type="text/css" href="/css/gallery.css">

</head>
<body>

	<?php echo HEADER ?>

	<main>
		<?php if (isset($_GET["filename"])): ?>
			<?php
				$filename = $DB->escape($_GET["filename"]);
				$picture = $DB->query("SELECT * FROM gallery WHERE filename = '$filename'")[0];
				$translation = $DB->query("CALL get_gallery_translation($picture[id], '$TRANSLATION->locale')")[0];
			?>
			<a href="/gallery" id="back"><?php echo $TRANSLATION["back"] ?></a>
			<img src="/img/<?php echo $filename ?>" id="picture">
			<div>
				<a href="/img/download/<?php echo $filename ?>" download><?php echo $TRANSLATION["download"] ?></a>
				<a href="/img/<?php echo $filename ?>"><?php echo $TRANSLATION["view"] ?></a>
			</div>
			<div id="metadata">
				<h1><?php echo $translation["title"] ?></h1>
				<div class="paragraph">
					<?php echo $translation["body"] ?>
				</div>
			</div>
		<?php else: ?> 
			<a href="/" id="back"><?php echo $TRANSLATION["back"] ?></a>
			<p>
				<?php echo $TRANSLATION["intro"] ?> 
			</p>
			<?php

				$page = isset($_GET["page"]) ? $DB->escape($_GET["page"]) : 0;
				$_pppage = CONFIG["gallery"]["pics_per_page"];

				$pages = $DB->query("
					SELECT CEILING(COUNT(id)/$_pppage) as n
					FROM gallery;
				")[0]["n"];

				$offset = $page * $_pppage;
				$pictures = $DB->query("
					SELECT *
					FROM gallery 
					ORDER BY post_date DESC, id DESC
					LIMIT $offset,$_pppage
				");

			?> 
			<div id="pages">
				<a href="/gallery" <?php if ($page===0) echo "disabled" ?>>0</a>
				<?php for ($i=1; $i < $pages; $i++): ?> 
					<a href="/gallery/<?php echo $i ?>" <?php if ($page==$i) echo "disabled" ?>>
						<?php echo $i ?> 
					</a>
				<?php endfor ?> 
			</div>
			<section id="gallery">
				<?php foreach ($pictures as $picture): ?> 
					<a href="/gallery/picture/<?php echo "$picture[filename]" ?>"
						class="picture" style="background-image: url(/img/<?php echo $picture["filename"] ?>);">
						<?php echo $picture["title"] ?> 
					</a>
				<?php endforeach ?> 
			</section>
		<?php endif ?> 
	</main>

	<?php echo FOOTER ?>

</body>
</html>
