#!/bin/bash

R="\033[33m"
G="\033[32m"
C="\033[0m"

ERR=0

if [[ $# == 1 ]]; then
	if [[ $1 == "--help" ]] || [[ $1 == "-h" ]]; then
		echo "Validates the xml files using lang.xsd"
		echo -e "\n\tUsage: $0 [XML]\n"
	else
		xmllint --schema lang.xsd $1 --noout
	fi
else
	for file in ??.xml; do
		name=$(xmllint --xpath "string(/translation/@name)" $file)
		out=$(xmllint --schema lang.xsd $file --noout 2>&1)
		if [[ $? == 0 ]]; then
			echo -e " [$file] $G$name$C validates!"
		else
			echo -e " [$file] $R$name$C doesn't validate!"
			((ERR+=1))
		fi
	done
fi

exit $ERR
