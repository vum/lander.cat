<?php $TRANSLATION->load("header") ?>
<div id="load" style="
	z-index: 100;
	position: fixed;
	top: 0; left: 0;
	width: 100vw;
	height: 100vh;
	background-color: var(--palette-bg);">
</div>

<header>
	<img src="/img/logo.png" width="160" alt="<?php echo $TRANSLATION["meta"]["face"] ?>">
	<h1>Facundo Lander</h1>
	<h2><?php echo $TRANSLATION["subtitle"] ?></h2>
</header>

<hr>
