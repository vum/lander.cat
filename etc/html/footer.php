<?php $TRANSLATION->load("footer") ?>
<hr>

<footer>
	<div>
		<a href="http://www.catb.org/hacker-emblem/" target="_blank" class="logo">
			<img src="/img/assets/hacker-emblem.bmp" alt="<?php echo $TRANSLATION["meta"]["hacker"] ?>">
		</a>
	</div>
	<div>
		<a href="https://en.wikipedia.org/wiki/Missionary_Church_of_Kopimism" target="_blank" class="logo">
			<img src="/img/assets/kopimi2.bmp" alt="<?php echo $TRANSLATION["meta"]["kopimi"] ?>">
			<img src="/img/assets/kopimi1.bmp" alt="<?php echo $TRANSLATION["meta"]["kopimi"] ?>">
		</a>
	</div>
	<p>&copy; 2021 - Facundo Lander</p>
	<a href="https://gitlab.com/vum/lander.cat"><?php echo $TRANSLATION["sorse"] ?></a>
	<div style="display: none" id="languages">
	<?php foreach (explode(",", CONFIG["language"]["locales"]) as $lang): ?>
		<a href="javascript:setLang(<?php echo "'$lang'" ?>)"><?php echo $lang ?></a>
	<?php endforeach ?></div>
	<script type="text/javascript">
		document.getElementById("languages").style.display = "block"
	</script>
	<noscript>
		<div>
		<?php foreach (explode(",", CONFIG["language"]["locales"]) as $lang): ?>
			<a href="/setlang/<?php echo $lang ?>"><?php echo $lang ?></a>
		<?php endforeach ?></div>
	</noscript>
</footer>
