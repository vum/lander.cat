<?php
/*
	Copyright 2021 Facundo Lander

	This file is part of lander.cat.

	lander.cat is free software: you can redistribute it and/or modify
	it under the terms of the GNU General Public License as published by
	the Free Software Foundation, either version 3 of the License, or
	(at your option) any later version.

	lander.cat is distributed in the hope that it will be useful,
	but WITHOUT ANY WARRANTY; without even the implied warranty of
	MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
	GNU General Public License for more details.

	You should have received a copy of the GNU General Public License
	along with lander.cat.  If not, see <https://www.gnu.org/licenses/>.
*/

if (isset($_GET["lang"])) {
	setcookie("lang", $_GET["lang"], time()+60*60*24*90, "/");
	header("Location: /");
	die();
}

class Translation implements ArrayAccess {

	public $provided;
	public $locale;

	// https://www.w3.org/Protocols/rfc2616/rfc2616-sec14.html
	private function negotiateLanguage() {		

		// BCP 47, Section 2.1.1
		$preferences = strtolower($_SERVER["HTTP_ACCEPT_LANGUAGE"]);
		$preferences = str_replace(" ", "", $preferences); // just in case...

		// ca,en-US;q=0.8,en;q=0.5,ko;q=0.3
		preg_match_all(
			"/[a-z]{2,3}(-[a-z]+)?(;q=[0-1]+\.[0-9])?/",
			$preferences, $preferences
		);
		$preferences = $preferences[0];

		$accepted = Array();

		foreach ($preferences as $preference) {
			$preference = explode(";", $preference);
			$locale = $preference[0];
			$quality = (sizeof($preference) === 1 ? 10 : substr($preference[1], 2)*10);
			$accepted[$quality] = $locale;
		}
		krsort($accepted);

		foreach ($accepted as $accept)
			if (in_array($accept, $this->provided))
				return $accept;

		return CONFIG["language"]["default"];
	}
	
	function __construct() {
		$this->provided = explode(",", CONFIG["language"]["locales"]);
		if (isset($_COOKIE["lang"]) and in_array($_COOKIE["lang"], $this->provided))
			$this->locale = $_COOKIE["lang"];
		else
			$this->locale = $this->negotiateLanguage();
		$xml = simplexml_load_file(ROOT."/etc/translation/$this->locale.xml", null, LIBXML_NOCDATA);
		$this->translation = json_decode(json_encode($xml), true);
		$this->selection = $this->translation;
	}

	// unused...
	function set(...$selector) {
		if (is_array($selector[0]))
			$selector = $selector[0];
		$this->selection = $this->selection ? $this->selection[$selector[0]] : null;
		if (sizeof($selector)>1)
			$this->set(array_shift($selector));
	}

	function load($selector) {
		$this->selection = $this->translation;
		$selectors = explode("/", $selector);
		foreach ($selectors as $selector)
			$this->selection = (array)$this->selection[$selector];
	}

	// ArrayAccess hooks START
	public function offsetGet($offset) {
		if (gettype($this->selection[$offset]) === "object") {
			return (array)$this->selection[$offset];
		} else {
			return $this->selection[$offset];
		}
	}
	public function offsetSet($offset, $value) {
		return False;
	}
	public function offsetExists($offset) {
		return isset($this->selection[$offset]);
	}
	public function offsetUnset($offset) {
		return False;
	}
	// ArrayAccess hooks END

}

?>