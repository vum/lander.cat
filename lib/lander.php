<?php
/*
	Copyright 2021 Facundo Lander

	This file is part of lander.cat.

	lander.cat is free software: you can redistribute it and/or modify
	it under the terms of the GNU General Public License as published by
	the Free Software Foundation, either version 3 of the License, or
	(at your option) any later version.

	lander.cat is distributed in the hope that it will be useful,
	but WITHOUT ANY WARRANTY; without even the implied warranty of
	MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
	GNU General Public License for more details.

	You should have received a copy of the GNU General Public License
	along with lander.cat.  If not, see <https://www.gnu.org/licenses/>.
*/

define("ROOT", $_SERVER["DOCUMENT_ROOT"]);
define("CONFIG", parse_ini_file(ROOT."/etc/config.ini", true));

require "Database.php";
require "Translation.php";
require "utils.php";


$DB = new Database();
$TRANSLATION = new Translation();

define("META", \utils\loadFile("/etc/html/meta.php"));
define("INCLUDES", \utils\loadFile("/etc/html/includes.php"));
define("HEADER", \utils\loadFile("/etc/html/header.php"));
define("FOOTER", \utils\loadFile("/etc/html/footer.php"));

?>