<?php
/*
	Copyright 2021 Facundo Lander

	This file is part of lander.cat.

	lander.cat is free software: you can redistribute it and/or modify
	it under the terms of the GNU General Public License as published by
	the Free Software Foundation, either version 3 of the License, or
	(at your option) any later version.

	lander.cat is distributed in the hope that it will be useful,
	but WITHOUT ANY WARRANTY; without even the implied warranty of
	MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
	GNU General Public License for more details.

	You should have received a copy of the GNU General Public License
	along with lander.cat.  If not, see <https://www.gnu.org/licenses/>.
*/

class Database {
	
	protected $mysql;

	function __construct() {
		$data = CONFIG["database"];
		$this->mysql = new mysqli("localhost", $data["user"], $data["pass"], $data["table"]);
	}

	function query($sql) {
		$out = $this->mysql->query($sql);
		if ($out->num_rows > 0) {
			$table = Array();
			while ($row = $out->fetch_assoc()) 
				array_push($table, $row);
			//return sizeof($table) === 1 ? $table[0] : $table;
			return $table;
		} else {
			return $out;
		}
	}

	function getID() {
		return $this->mysql->insert_id;
	}

	function escape($var) {
		return $this->mysql->real_escape_string($var);
	}

}

?>